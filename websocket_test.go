package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/gorilla/websocket"
	"github.com/stretchr/testify/require"
)

func TestWebsocketProxy(t *testing.T) {
	var wg sync.WaitGroup

	wg.Add(1)
	server := httptest.NewServer(http.HandlerFunc(func(wr http.ResponseWriter, req *http.Request) {
		defer wg.Done()
		upgrader := websocket.Upgrader{}
		conn, err := upgrader.Upgrade(wr, req, http.Header{})
		require.NoError(t, err)
		require.NoError(t, conn.WriteMessage(websocket.TextMessage, []byte("hi")))
	}))
	defer server.Close()

	wsProxy := &websocketProxy{
		upgrader: websocket.Upgrader{
			HandshakeTimeout: 3 * time.Second,
		},
		dialer:      websocket.DefaultDialer,
		upstreamURL: "ws://" + strings.TrimPrefix(server.URL, "http://"),
	}
	proxy := httptest.NewServer(wsProxy)
	defer proxy.Close()

	clientConn, _, err := websocket.DefaultDialer.Dial("ws://"+strings.TrimPrefix(proxy.URL, "http://"), http.Header{})
	require.NoError(t, err)

	msgType, data, err := clientConn.ReadMessage()
	require.NoError(t, err)
	require.Equal(t, websocket.TextMessage, msgType)
	require.Equal(t, []byte("hi"), data)
	require.NoError(t, clientConn.Close())

	wg.Wait()
}
