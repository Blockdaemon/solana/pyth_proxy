FROM golang:alpine as builder
WORKDIR /app
COPY . .
RUN go build -ldflags="-w -s" -o /app/pyth_proxy .

FROM alpine
COPY --from=builder /app/pyth_proxy /pyth_proxy
ENTRYPOINT ["/pyth_proxy"]
