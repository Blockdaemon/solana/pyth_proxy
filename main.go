package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"net/http/httputil"
	"net/http/pprof"
	"net/url"
	"os"
	"time"

	"github.com/gorilla/websocket"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"golang.org/x/sync/errgroup"
)

func main() {
	nodeFlag := flag.String("node", "", "Node domains")
	authToken := flag.String("auth-token", "", "Auth token")
	rpcListen := flag.String("rpc-listen", ":8899", "RPC port")
	wsListen := flag.String("ws-listen", ":8900", "WebSocket port")
	metricsListen := flag.String("metrics-listen", "", "Metrics port (omit to disable)")
	flag.Parse()
	if *nodeFlag == "" {
		flag.Usage()
		os.Exit(1)
	}
	if tokenFromEnv := os.Getenv("SOLANA_AUTH_TOKEN"); tokenFromEnv != "" {
		*authToken = tokenFromEnv
	}
	if *authToken == "" {
		flag.Usage()
		os.Exit(1)
	}

	rpcURL := url.URL{
		Scheme: "https",
		Host:   *nodeFlag + ":443",
	}
	wsURL := url.URL{
		Scheme: "wss",
		Host:   *nodeFlag + ":8443",
		Path:   "/websocket",
	}

	rpcProxy := httputil.NewSingleHostReverseProxy(&rpcURL)
	rpcProxy.Director = func(req *http.Request) {
		metricHttpRequests.Inc()
		*req.URL = rpcURL
	}
	wsProxy := &websocketProxy{
		upgrader: websocket.Upgrader{
			HandshakeTimeout: 3 * time.Second,
		},
		dialer:      websocket.DefaultDialer,
		upstreamURL: wsURL.String(),
		token:       *authToken,
	}

	group, ctx := errgroup.WithContext(context.Background())
	if *metricsListen != "" {
		group.Go(func() error {
			log.Print("Listening for metrics requests on ", *metricsListen)
			return listenAndServeContext(ctx, *metricsListen, metricsHandler())
		})
	}
	group.Go(func() error {
		log.Print("Listening for RPC on ", *rpcListen)
		return listenAndServeContext(ctx, *rpcListen, &authenticator{
			upstream: rpcProxy,
			token:    *authToken,
		})
	})
	group.Go(func() error {
		log.Print("Listening for WebSockets on ", *wsListen)
		return listenAndServeContext(ctx, *wsListen, &authenticator{
			upstream: wsProxy,
			token:    *authToken,
		})
	})
	log.Print("Running pyth_proxy")
	if err := group.Wait(); err != nil {
		log.Fatal(err)
	}
}

type authenticator struct {
	upstream http.Handler
	token    string
}

func (a *authenticator) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	req.Header.Set("authorization", "Bearer "+a.token)
	a.upstream.ServeHTTP(rw, req)
}

func listenAndServeContext(ctx context.Context, listen string, handler http.Handler) error {
	server := &http.Server{
		Addr:    listen,
		Handler: handler,
	}
	go func() {
		defer server.Close()
		<-ctx.Done()
	}()
	return server.ListenAndServe()
}

func metricsHandler() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	mux.Handle("/metrics", promhttp.HandlerFor(metricsRegistry, promhttp.HandlerOpts{
		ErrorLog:          log.Default(),
		EnableOpenMetrics: true,
	}))
	return mux
}
