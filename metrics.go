package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var metricsRegistry = prometheus.NewRegistry()
var metricsFactory = promauto.With(metricsRegistry)

const namespace = "pyth_proxy"

const (
	labelDirection = "send_direction"
	labelFrameType = "frame_type"
)

const (
	directionUpstream   = "upstream"
	directionDownstream = "downstream"
)

const (
	frameText   = "text"
	frameBinary = "binary"
	framePing   = "ping"
	framePong   = "pong"
	frameOther  = "other"
)

var (
	metricHttpRequests = metricsFactory.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "http_requests",
		Help:      "Number of requests made to HTTP port",
	})
	metricWSConns = metricsFactory.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "websocket_active_conns",
		Help:      "Number of active proxied WebSocket conns",
	})
	metricWSMessages = metricsFactory.NewCounterVec(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "websocket_messages",
		Help:      "Number of proxied WebSocket messages",
	}, []string{labelDirection, labelFrameType})
)
