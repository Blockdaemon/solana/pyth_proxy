package main

import (
	"context"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"golang.org/x/sync/errgroup"
)

type websocketProxy struct {
	upgrader    websocket.Upgrader
	dialer      *websocket.Dialer
	upstreamURL string
	token       string
}

func (p *websocketProxy) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	downstreamConn, err := p.upgrader.Upgrade(rw, req, http.Header{})
	if err != nil {
		log.Print("Failed to upgrade incoming request to WebSocket ", err)
		return
	}
	defer downstreamConn.Close()
	instrumentWSConn(downstreamConn, false)

	upstreamConn, _, err := p.dialer.Dial(p.upstreamURL, http.Header{
		"Authorization": {"Bearer " + p.token},
	})
	if err != nil {
		log.Print("Failed to dial upstream WebSocket ", err)
		return
	}
	defer upstreamConn.Close()
	instrumentWSConn(upstreamConn, true)

	metricWSConns.Inc()
	defer metricWSConns.Dec()

	group, ctx := errgroup.WithContext(context.Background())

	upstreamC := make(chan outgoingMsg)
	group.Go(func() error {
		return readMessages(ctx, downstreamConn, upstreamC)
	})
	group.Go(func() error {
		pinger(ctx, upstreamC)
		return nil
	})
	group.Go(func() error {
		return forwardMessages(ctx, upstreamC, upstreamConn, true)
	})

	downstreamC := make(chan outgoingMsg)
	group.Go(func() error {
		return readMessages(ctx, upstreamConn, downstreamC)
	})
	group.Go(func() error {
		pinger(ctx, downstreamC)
		return nil
	})
	group.Go(func() error {
		return forwardMessages(ctx, downstreamC, downstreamConn, false)
	})

	<-ctx.Done()
	_ = downstreamConn.Close()
	_ = upstreamConn.Close()

	if err := group.Wait(); err != nil {
		log.Print("WebSocket proxy stopped ", err)
	}
}

const wsWriteWait = time.Second

func wsDirection(upstream bool) string {
	if upstream {
		return directionUpstream
	} else {
		return directionDownstream
	}
}

func wsFrame(msgType int) string {
	switch msgType {
	case websocket.TextMessage:
		return frameText
	case websocket.BinaryMessage:
		return frameBinary
	case websocket.PingMessage:
		return framePing
	case websocket.PongMessage:
		return framePong
	default:
		return frameOther
	}
}

func instrumentWSConn(conn *websocket.Conn, upstream bool) {
	conn.SetPingHandler(func(appData string) error {
		return handleWSPing(conn, appData, upstream)
	})
	conn.SetPongHandler(func(_ string) error {
		return handleWSPong(upstream)
	})
}

func handleWSPing(conn *websocket.Conn, appData string, upstream bool) error {
	metricWSMessages.WithLabelValues(wsDirection(upstream), framePing).Inc()

	err := conn.WriteControl(websocket.PongMessage, []byte(appData), time.Now().Add(wsWriteWait))
	if err == websocket.ErrCloseSent {
		return nil
	} else if e, ok := err.(net.Error); ok && e.Temporary() {
		return nil
	}
	return err
}

func handleWSPong(upstream bool) error {
	metricWSMessages.WithLabelValues(wsDirection(upstream), framePong).Inc()
	return nil
}

type outgoingMsg struct {
	msgType int
	data    []byte
}

func readMessages(ctx context.Context, fromConn *websocket.Conn, out chan<- outgoingMsg) error {
	for {
		msgType, data, err := fromConn.ReadMessage()
		if err != nil {
			return err
		}
		msg := outgoingMsg{msgType: msgType, data: data}
		select {
		case <-ctx.Done():
			return nil
		case out <- msg:
		}
	}
}

func pinger(ctx context.Context, out chan<- outgoingMsg) {
	const pingInterval = time.Second
	ticker := time.NewTicker(pingInterval)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
		}

		msg := outgoingMsg{msgType: websocket.PingMessage, data: nil}
		select {
		case <-ctx.Done():
			return
		case out <- msg:
		}
	}
}

func forwardMessages(ctx context.Context, out <-chan outgoingMsg, toConn *websocket.Conn, upstream bool) error {
	for {
		select {
		case <-ctx.Done():
			return nil
		case msg := <-out:
			metricWSMessages.WithLabelValues(wsDirection(upstream), wsFrame(msg.msgType)).Inc()
			if err := toConn.WriteMessage(msg.msgType, msg.data); err != nil {
				return err
			}
		}
	}
}
